package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

/**
 * Класс с тестами для {@link MyBranching}
 */

public class MyBranchingTest
{
    /**
     * Проверяем успешный сценарий utilFunc2()
     * */
    @Test
    public void ifElseExample_Success_Test()
    {
        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        boolean result = myBranching.ifElseExample();

        Assertions.assertTrue(result);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());
    }

    /**
     * Проверяем не успешный сценарий utilFunc2()
     * */
    @Test
    public void ifElseExample_Fail_Test()
    {
        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);

        boolean result = myBranching.ifElseExample();

        Assertions.assertFalse(result);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());
    }


    /**
     * Проверяем успешный сценарий utilFunc2() для метода MyBranching.maxInt(int i1, int i2)
     * */
    @Test
    public void maxInt_SuccessUtilFunc2_Test()
    {
        final int i1 = 2;
        final int i2 = 5;

        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        int result = myBranching.maxInt(i1, i2);

        Assertions.assertEquals(0, result);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }


    /**
     * Проверяем не успешный сценарий utilFunc2() для метода MyBranching.maxInt(int i1, int i2),
     * на вход поступают разные цифры
     * */
    @Test
    public void maxInt_FailUtilFunc2_differentNumbers_Test()
    {
        final int min = 2;
        final int max = 5;

        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);

        int result = myBranching.maxInt(min, max);

        Assertions.assertEquals(max, result);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    /**
     * Проверяем не успешный сценарий utilFunc2() для метода MyBranching.maxInt(int i1, int i2),
     * на вход поступают равные цифры
     * */
    @Test
    public void maxInt_FailUtilFunc2_equalNumbers_Test()
    {
        final int i1 = 2;
        final int i2 = 2;

        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);

        int maxInt = myBranching.maxInt(i1, i2);

        boolean result = (maxInt == i1);

        Assertions.assertTrue(result);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    /**
     * Проверяем результат switchExample при поступлении на вход 0 и успешном сценарии utilFunc2()
     * */
    @Test
    public void switchExample_SuccessUtilFunc2_0_Test()
    {
        final int i = 0;

        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.switchExample(i);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(Mockito.anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }



    /**
     * Проверяем результат switchExample при поступлении на вход 0 и не успешном сценарии utilFunc2()
     * */
    @Test
    public void switchExample_FailUtilFunc2_0_Test()
    {
        final int i = 0;

        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.switchExample(i);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());
    }

    /**
     * Проверяем результат switchExample при поступлении на вход 1
     * */
    @Test
    public void switchExample_1_Test()
    {
        final int i = 1;

        Utils utilsMock = Mockito.mock(Utils.class);

        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.switchExample(i);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(Mockito.anyString());
    }

    /**
     * Проверяем результат switchExample при поступлении на вход 2
     * */
    @Test
    public void switchExample_2_Test()
    {
        final int i = 2;

        Utils utilsMock = Mockito.mock(Utils.class);

        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.switchExample(i);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());
    }
}

